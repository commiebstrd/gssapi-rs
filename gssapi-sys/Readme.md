Rust FFI bindings to gssapi via bindgen.

See [gssapi crate](https://crates.io/crates/gssapi) or [gssapi repository](https://gitlab.com/commiebstrd/gssapi-rs) for safe Rust versions.
