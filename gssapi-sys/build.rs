extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
  let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
  println!("cargo:rustc-link-lib=gssapi_krb5");

  let gss_types = "(OM_.+|gss_.+)";
  let gss_vars = "GSS_.+";
  let gss_funcs = "gss_.*";

  let gssapi = bindgen::Builder::default()
    .header("includes/gssapi.h");

  #[cfg(target_os="macos")]
  let gssapi = gssapi.header("includes/osx.h");

  let gssapi = gssapi.whitelist_type(gss_types)
    .whitelist_var(gss_vars)
    .whitelist_function(gss_funcs)
    .generate()
    .expect("Unable to generate <gssapi.h> bindings");
  gssapi.write_to_file(out_path.join("gssapi.rs")).expect("Couldn't write <gssapi.h> bindings!");
}
