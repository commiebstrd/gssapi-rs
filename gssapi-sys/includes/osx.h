/**
 * <div rustbindgen replaces="gss_OID_desc_struct"></div>
 */
struct gss_oid_desc_struct {
    OM_uint32 length;
    void *elements;
};

/**
 * <div rustbindgen replaces="gss_buffer_desc_struct"></div>
 */
struct gss_BUFFER_desc_struct {
    size_t length;
    void *value;
};

/**
 * <div rustbindgen replaces="gss_channel_bindings_struct"></div>
 */
typedef struct gss_CHANNEL_bindings_struct {
    OM_uint32 initiator_addrtype;
    gss_buffer_desc initiator_address;
    OM_uint32 acceptor_addrtype;
    gss_buffer_desc acceptor_address;
    gss_buffer_desc application_data;
};
