### gssapi
#### Safe rust overlay for gssapi-sys
#### Spenser Reinhardt <commiebstrd@protonmail.com>

Rust code for interacting with gssapi via normal safe rust code.

*NOTE* This does not use crates.io gssapi-sys, this package uses the local bindgen version to dynamically compile and link against your version of gssapi directly.
