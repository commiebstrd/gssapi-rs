#[macro_use] extern crate enum_primitive;
#[macro_use] extern crate lazy_static;
extern crate num;
extern crate gssapi_sys;

mod errors;
mod constants;
mod types;
mod functions;
#[cfg(test)] mod tests;

pub mod prelude {
  pub use ::errors::*;
  pub use ::types::*;
  pub use ::constants::*;
  pub use ::functions::*;
}

#[test]
fn included() {
    assert_eq!(2 + 2, 4);
}
