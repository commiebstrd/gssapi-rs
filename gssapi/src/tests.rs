use ::prelude::*;
use gssapi_sys;

#[test]
fn included() {
    assert_eq!(2 + 2, 4);
}

macro_rules! const_test {
  // checks to and from int types, as well as sys types
  ($base:ty, $name:path, $val:expr, $sys:path) => {
    assert_eq!{Into::<$base>::into($val), $name};     // int val -> this type
    assert_eq!{Into::<OM_uint32>::into($name), $val}; // this type -> int val
    assert_eq!{Into::<OM_uint32>::into($name), $sys}; // this type -> sys val
    assert_eq!{Into::<$base>::into($sys), $name};     // sys val -> this type
  };
  // checks conversions to and from int only, no sys types
  ($base:ty, $name:path, $val:expr) => {
    assert_eq!{Into::<$base>::into($val), $name}; // int val -> this type
    assert_eq!{Into::<OM_uint32>::into($name), $val}; // this type -> int val
  }
}

#[test]
fn GSS_Context_Flag() {
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Delegate, 1, gssapi_sys::GSS_C_DELEG_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Mutual, 2, gssapi_sys::GSS_C_MUTUAL_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Replay, 4, gssapi_sys::GSS_C_REPLAY_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Sequence, 8, gssapi_sys::GSS_C_SEQUENCE_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Conf, 16, gssapi_sys::GSS_C_CONF_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Integ, 32, gssapi_sys::GSS_C_INTEG_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Anonymous, 64, gssapi_sys::GSS_C_ANON_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::ProtReady, 128, gssapi_sys::GSS_C_PROT_READY_FLAG};
  const_test!{GSS_Context_Flag, GSS_Context_Flag::Trans, 256, gssapi_sys::GSS_C_TRANS_FLAG};
}

#[test]
fn GSS_Credential_Flag() {
  const_test!{GSS_Credential_Flag, GSS_Credential_Flag::Both, 0, gssapi_sys::GSS_C_BOTH};
  const_test!{GSS_Credential_Flag, GSS_Credential_Flag::Initiate, 1, gssapi_sys::GSS_C_INITIATE};
  const_test!{GSS_Credential_Flag, GSS_Credential_Flag::Accept, 2, gssapi_sys::GSS_C_ACCEPT};
}

#[test]
fn GSS_Display_Flag() {
  const_test!{GSS_Display_Flag, GSS_Display_Flag::GssCode, 1, gssapi_sys::GSS_C_GSS_CODE};
  const_test!{GSS_Display_Flag, GSS_Display_Flag::MechCode, 2, gssapi_sys::GSS_C_MECH_CODE};
}

#[test]
fn GSS_Channel_Flag() {
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Unspec, 0, gssapi_sys::GSS_C_AF_UNSPEC};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Local, 1, gssapi_sys::GSS_C_AF_LOCAL};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Inet, 2, gssapi_sys::GSS_C_AF_INET};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Implink, 3, gssapi_sys::GSS_C_AF_IMPLINK};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Pup, 4, gssapi_sys::GSS_C_AF_PUP};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Chaos, 5, gssapi_sys::GSS_C_AF_CHAOS};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::NS, 6, gssapi_sys::GSS_C_AF_NS};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::NBS, 7, gssapi_sys::GSS_C_AF_NBS};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::ECMA, 8, gssapi_sys::GSS_C_AF_ECMA};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Datakit, 9, gssapi_sys::GSS_C_AF_DATAKIT};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::CCITT, 10, gssapi_sys::GSS_C_AF_CCITT};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::SNA, 11, gssapi_sys::GSS_C_AF_SNA};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::DECnet, 12, gssapi_sys::GSS_C_AF_DECnet};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::DLI, 13, gssapi_sys::GSS_C_AF_DLI};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::LAT, 14, gssapi_sys::GSS_C_AF_LAT};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Hylink, 15, gssapi_sys::GSS_C_AF_HYLINK};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Appletalk, 16, gssapi_sys::GSS_C_AF_APPLETALK};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::BSC, 17, gssapi_sys::GSS_C_AF_BSC};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::DSS, 18, gssapi_sys::GSS_C_AF_DSS};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::OSI, 19, gssapi_sys::GSS_C_AF_OSI};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::X25, 21, gssapi_sys::GSS_C_AF_X25};
  const_test!{GSS_Channel_Flag, GSS_Channel_Flag::Null, 255, gssapi_sys::GSS_C_AF_NULLADDR};
}

#[test]
fn GSS_KRB5_Flag() {
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::BadServiceName, 1};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::BadStringUID, 2};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::NoUser, 3};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::ValidateFailed, 4};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::BufferAlloc, 5};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::BadMsgCTX, 6};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::WrongSize, 7};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::BadUsage, 8};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::UnknownQOP, 9};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::CCacheNoMatch, 10};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::KeyTabNoMatch, 11};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::TGTMissing, 12};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::NoSubKey, 13};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::ContextEstablished, 14};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::BadSignType, 15};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::BadLength, 16};
  const_test!{GSS_KRB5_Flag, GSS_KRB5_Flag::CTXIncomplete, 17};
}

macro_rules! static_test {
  ($name:expr, $val:expr) => {
      let val = $val.to_vec();
      assert_eq!{val.len(), $name.length() as usize};
      assert_eq!{&val, $name.elements()};
  }
}

#[test]
fn GSS_NT_LAZY_STATICS() {
  static_test!{GSS_C_NT_USER_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x01]};
  static_test!{GSS_C_NT_MACHINE_UID_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x02]};
  static_test!{GSS_C_NT_STRING_UID_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x03]};
  static_test!{GSS_C_NT_HOSTBASED_SERVICE, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x04]};
  static_test!{GSS_C_NT_HOSTBASED_SERVICE_X, [0x2b, 0x06, 0x01, 0x05, 0x06, 0x02]};
  static_test!{GSS_C_NT_ANONYMOUS, [0x2b, 0x06, 0x01, 0x05, 0x06, 0x03]};
  static_test!{GSS_C_NT_EXPORT_NAME, [0x2b, 0x06, 0x01, 0x05, 0x06, 0x04]};
}

#[test]
fn GSS_KRB5_LAZY_STATICS() {
  static_test!{GSS_KRB5_NT_USER_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x01]};
  static_test!{GSS_KRB5_NT_MACHINE_UID_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x02]};
  static_test!{GSS_KRB5_NT_STRING_UID_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x03]};
  static_test!{GSS_KRB5_NT_HOSTBASED_SERVICE_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x01, 0x04]};
  static_test!{GSS_KRB5_NT_PRINCIPAL_NAME, [0x2a, 0x86, 0x48, 0x86, 0xf7, 0x12, 0x01, 0x02, 0x02, 0x01]};
}
